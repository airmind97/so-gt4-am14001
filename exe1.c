#include <stdio.h>

int main()
{
  float n1;
  float n2;
  float *p1;
  float *p2;

  n1 = 4.0;
  p1 = &n1;       // *p1 = n1
  p2 = p1;        // *p2 = n1;
  n2 = *p2;       // n2 = n1 = 4.0;
  n1 = *p1 + *p2; // n1 = n1 + n1 = 4.0 + 4.0 = 8.0

  printf("n1: %f, n2: %f\n", n1, n2);
}
