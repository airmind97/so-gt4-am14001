#include <stdio.h>
#include <stdlib.h>

int sortDesc(int *start, int *length)
{
  // Sort the numbers in an "array" starting at start and with length length, in descendent order
  int j;
  for (j = 0; j < *length; j++)
  {
    int k;
    for (k = j + 1; k < *length; k++)
    {
      if (*(start + j) < *(start + k))
      {
        int tmp = *(start + j);
        *(start + j) = *(start + k);
        *(start + k) = tmp;
      }
    }
  }
}

int main()
{
  // Allocate space for an int to store the quantity of values
  int *qty = malloc(sizeof(int));

  // Ask the user for how many values does want to save
  printf("How many values do you want to use? ");
  scanf("%d", qty);

  // Allocate space for the numbers array
  int *numbers = malloc(sizeof(int) * *qty);

  printf("\nRandom values\n");

  // Fill the array with random values between 0 and 100
  int i;
  for (i = 0; i < *qty; i++)
  {
    *(numbers + i) = rand() % 101;
    printf("maddr: %p, val: %d\n", (numbers + i), *(numbers + i));
  }

  sortDesc(numbers, qty);

  // Print ordered values
  printf("\nDesc order\n");

  int l;
  for (l = 0; l < *qty; l++)
  {
    printf("maddr: %p, val: %d\n", (numbers + l), *(numbers + l));
  }
}
