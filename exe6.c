#include <stdio.h>
#include <stdlib.h>

// Return the maximum value of an "array" that starts at start and has length length
float max(float *start, int *length)
{
  float max = *start;
  int i;
  for (i = 1; i < *length; i++)
  {
    if (*(start + i) > max)
    {
      max = *(start + i);
    }
  }

  return max;
}

// Return the minimun value of an "array" that starts at start and has length length
float min(float *start, int *length)
{
  float min = *start;
  int i;
  for (i = 1; i < *length; i++)
  {
    if (*(start + i) < min)
    {
      min = *(start + i);
    }
  }

  return min;
}

// Return the average of an "array" that starts at start and has length length
float avg(float *start, int *length)
{
  float sum = 0;

  int i;
  for (i = 0; i < *length; i++)
  {
    sum += *(start + i);
  }

  return (sum / *length);
}

int main()
{
  // Allocate space for an int to store the quantity of values
  int *qty = malloc(sizeof(int));

  // Ask the user for how many values does want to save
  printf("How many values do you want to use? ");
  scanf("%d", qty);

  // Allocate space to store the values to be entered by the user
  float *numbers = malloc(sizeof(float) * *qty);

  // Ask the user for the values
  int i;
  for (i = 0; i < *qty; i++)
  {
    printf("Enter a value: ");
    scanf("%f", (numbers + i));
  }

  printf("\nmax: %f, min: %f, avg: %f\n", max(numbers, qty), min(numbers, qty), avg(numbers, qty));
}
