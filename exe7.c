#include <stdio.h>
#include <stdlib.h>
#include <math.h>

// Evaluate a polynomial with one variable
float evalPolynomial(float *coefficients, float *x, int *degree)
{
  float result = 0;

  int i;
  for (i = 0; i <= *degree; i++)
  {
    result += (*(coefficients + i) * pow(*x, i));
  }

  return result;
}

int main()
{
  // Allocate space for an int to store the degree of the polynomial
  int *polynomialDegree = malloc(sizeof(int));

  // Ask the user for the degree of the polynomial
  printf("What's the degree of the polynomial? ");
  scanf("%d", polynomialDegree);

  // Allocate space to store the coefficients of the polynomial
  // (*polynomialDegree + 1) because the number of terms of a polynomial is always 1 more than the degree of said polynomial
  float *coefficients = malloc(sizeof(float) * (*polynomialDegree + 1));

  // Ask the user for the coefficients
  // (i <= *polynomialDegree) because the number of terms of a polynomial is always 1 more than the degree of said polynomial
  int i;
  for (i = 0; i <= *polynomialDegree; i++)
  {
    printf("Enter coefficient for x^%d: ", i);
    scanf("%f", (coefficients + i));
  }

  // Allocate space to store x value
  float *x = malloc(sizeof(float));

  // Ask user for x value
  printf("\nWhat's x value? ");
  scanf("%f", x);

  printf("\nResult: %f\n", evalPolynomial(coefficients, x, polynomialDegree));
}
